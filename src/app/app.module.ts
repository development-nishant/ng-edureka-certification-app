import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { RegistrationComponent } from './registration/registration.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { FriendListComponent } from './friend-list/friend-list.component';
import { NetworkComponent } from './network/network.component';
import { SettingsComponent } from './settings/settings.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationComponent } from './authentication/authentication.component';
import { ResetComponent } from './reset/reset.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';
import {HttpInterceptorService} from './shared/services/http-interceptor.service';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { PostReducer } from './reducers/post.reducer';
import { NgrxDemoComponent } from './ngrx-demo/ngrx-demo.component';
import { TopicReducer } from './reducers/topics.reducer';
import { CustomDecoratorsComponent } from './ng-features/decorators-demo/custom-decorators/custom-decorators.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    RegistrationComponent,
    HomeComponent,
    LoginComponent,
    ForgotpasswordComponent,
    FriendListComponent,
    NetworkComponent,
    SettingsComponent,
    AuthenticationComponent,
    ResetComponent,
    ProfileComponent,
    NgrxDemoComponent,
    CustomDecoratorsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    /*StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    */
    StoreModule.forRoot({
      _post_state: PostReducer,
      _topics_state : TopicReducer
    }, {})
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,useClass:HttpInterceptorService,multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
