
import { Post } from './../utils/post';
import { createAction, props, Action } from '@ngrx/store';
import { allowedNodeEnvironmentFlags } from 'process';

/*
export const loadPosts = createAction(
  '[Post] Load Posts'
);

export const loadPostsSuccess = createAction(
  '[Post] Load Posts Success',
  props<{ data: any }>()
);

export const loadPostsFailure = createAction(
  '[Post] Load Posts Failure',
  props<{ error: any }>()
);
*/
export const ADD_POST = "[Post] Add";
export const LOAD_POST = "[Post] Load";

export class AddPostAction implements Action{
  readonly type: string = ADD_POST;
  constructor(public postDetail : Post){

  }
}

//tslint One class is allowedNodeEnvironmentFlags...

/*export class LoadPosts implements Action{
  readonly type:string = LOAD_POST;

  constructor(){
  }
}*/

export type _actions = AddPostAction;
