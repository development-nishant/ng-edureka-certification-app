
import { Topic } from '../utils/topic';
import { Action } from '@ngrx/store';

export const ADD_TOPIC = "[Topic] Add";

export class AddTopicAction implements Action {
  readonly type: string = ADD_TOPIC;
  constructor(public topicDetail : any){
  }
}

export type _actions = AddTopicAction;
