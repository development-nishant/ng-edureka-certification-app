import { Post } from './utils/post';
import { Topic } from './utils/topic';
export interface AppState {

  readonly _post_state: Post[];
  readonly _topics_state: Topic[];
}
