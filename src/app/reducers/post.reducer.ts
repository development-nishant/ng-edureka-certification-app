
import { Post } from './../utils/post';
import * as PostActions from '../actions/post.actions';
import { NgSwitchCase } from '@angular/common';


export function PostReducer(_post_state:Post[]=[] , action: PostActions._actions){
  switch(action.type){
    case PostActions.ADD_POST:
      // add new post details to existing array of post state...
      return [..._post_state,action.postDetail];
    default:
      return _post_state;
  }
}

