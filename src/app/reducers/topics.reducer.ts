import * as TopicsActions from '../actions/topics.actions';
import { Topic } from '../utils/topic';

export const initVal= {
  topicId:"**",
  topicName :"Initial Value in store"
}
export function TopicReducer(store:Topic[]=[initVal],action:TopicsActions._actions){
  switch(action.type){
    case TopicsActions.ADD_TOPIC:
      return [...store,action.topicDetail];
    default:
      return store;
  }
}
