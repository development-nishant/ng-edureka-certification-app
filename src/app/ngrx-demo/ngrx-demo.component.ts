import { Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Topic } from '../utils/topic';
import { HomeService } from '../home/home.service';
import { AppState } from '../app.state';
import { Store } from '@ngrx/store';
import  * as TopicsActions from "../actions/topics.actions";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-ngrx-demo',
  templateUrl: './ngrx-demo.component.html',
  styleUrls: ['./ngrx-demo.component.css']
})
export class NgrxDemoComponent implements OnInit {


  createNewNgRxForm: FormGroup;
  allTopics:Observable<any[]>;
  title:string = "Topics...";
  constructor(private homeService:HomeService , private store : Store<AppState>) {
    this.allTopics = store.select('_topics_state');
  }

  ngOnInit(): void {
    this.renderCreateNewForm();
  }
  createNewTopic() {
    let topicObj = {
      topicId:"10057",
      topicName:this.createNewNgRxForm.value['topicTxt']
    };
    this.store.dispatch(new TopicsActions.AddTopicAction(topicObj));
  }
  fetchAllPosts () :void{
    this.homeService.fetchAllPosts().subscribe((resp:any)=>{
      this.allTopics = resp;
    },(err:any)=>{});
  }
  renderCreateNewForm() {
    this.createNewNgRxForm = new FormGroup({
      topicTxt: new FormControl(null, Validators.required)
    });
  }
}
