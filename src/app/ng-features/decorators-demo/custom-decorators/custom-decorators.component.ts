import { Component, OnInit } from '@angular/core';
export function classCustomDecorator(target : Function){
  debugger;

}
export function methodCustomDecorator(target: Object, propertyKey: string, descriptor: PropertyDescriptor){
  debugger;

}

export function propertyCustomDecorator(target: Object, propertyName: string){
  debugger;
 /* target — current object’s prototype i.e — If Employee is an Object, Employee.prototype
propertyKey — name of the method
index — position of the parameter in the argument array
*/
}
type Constructor<T = {}> = new (...args: any[]) => T;

function Timestamped<TBase extends Constructor>(Base: TBase) {
  return class extends Base {
    timestamp = Date.now();
  };
}
@Component({
  selector: 'app-custom-decorators',
  templateUrl: './custom-decorators.component.html',
  styleUrls: ['./custom-decorators.component.css']
})


@classCustomDecorator
export class CustomDecoratorsComponent implements OnInit {

  @propertyCustomDecorator
  prop1:string;

  constructor() {
    this.prop1 = "Hello";
  }

  ngOnInit(): void {
    let atype = <number> 445;

    this.method1();
  }

  @methodCustomDecorator
  method1(){
    console.log("Inside method1 : "+this.prop1);
    debugger;
  }
}
